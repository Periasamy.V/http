import { Component } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HttpService } from './http.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'sample';
  todo: any;

  ngOnInit() {}

  constructor(private http: HttpClient) {}

  onCreatePost(postData: { title: string; content: string }) {
    // Send Http request
    this.http
      .post(
        'https://sample-9b460-default-rtdb.firebaseio.com/data.json',
        postData
      )
      .subscribe((responseData) => {
        console.log(responseData);
      });
  }

  onShow() {
    this.http
      .get('https://sample-9b460-default-rtdb.firebaseio.com/data.json')
      .subscribe((data: any) => {
        console.table(data);
        this.todo = data;
      });
  }


  onDelete(){

      // Send Http request
      this.http
        .delete(
          'https://sample-9b460-default-rtdb.firebaseio.com/data.json',
          // deleteData
        )
        .subscribe((responseData) => {
          console.log(responseData);
        });
    
  }
}
